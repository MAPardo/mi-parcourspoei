from tkinter import Tk, filedialog, ttk, messagebox
from tkinter import Frame, Label, Button, Listbox, Checkbutton, IntVar, Entry

from local import fonctions_listing as fl
from local import fonctions_synchro as fs
from bdd import fonctions_sql as fsql
import time
from ftplib import FTP
from ftp import fonctions_synchro_ftp as fs_ftp
from ftp import fonctions_listing_ftp as fl_ftp

class Application(Tk):
    def __init__(self,info_bdd):
        Tk.__init__(self)
        self.info_bdd = info_bdd
        self.creer_widgets()

    def creer_widgets(self):

        # -------------------- Sélection du type de synchro -------------------- #

        self.frame1 = Frame(master=self, background='lemon chiffon')
        self.frame1.grid_columnconfigure(0, weight=1)
        self.frame1.grid_rowconfigure(0, weight=1)

        self.label_methods = Label(
            self.frame1, text="Sélectionner la méthode de synchronisation: ", background='lemon chiffon')

        self.list_methods = Listbox(self.frame1, height=2, width=10)
        self.list_methods.insert(1, "local")
        self.list_methods.insert(2, "ftp")
        self.list_methods.select_set(0)

        self.button_type = Button(self.frame1, text="Valider", width=10,
                                  command=self.VerifTypeSynchro, background='lemon chiffon')

        self.frame1.grid(row=0, column=0, ipadx=10, ipady=10, sticky='new')
        self.label_methods.grid(row=0, column=0, columnspan=3, padx=10, pady=5)
        self.list_methods.grid(row=1, column=0, columnspan=2, padx=10, pady=5)
        self.button_type.grid(row=2, column=0, columnspan=3, padx=10, pady=5)

# -------------------- Sélection du type de fichier à synchro -------------------- #

        self.label_ext = Label(
            self.frame1, text="Sélectionner le type de fichier à synchro: ", background='lemon chiffon')

        self.ext_txt = IntVar()
        self.ext_html = IntVar()
        self.ext_all = IntVar(value=1)

        self.list_ext_txt = Checkbutton(
            self.frame1, text='TXT', variable=self.ext_txt, onvalue=1, offvalue=0, background='lemon chiffon')
        self.list_ext_html = Checkbutton(
            self.frame1, text='HTML', variable=self.ext_html, onvalue=1, offvalue=0, background='lemon chiffon')
        self.list_ext_all = Checkbutton(
            self.frame1, text='all', variable=self.ext_all, onvalue=1, offvalue=0, background='lemon chiffon')

        self.label_ext_input = Label(
            self.frame1, text="Autre : ", background='lemon chiffon')
        self.ext_input = Entry(self.frame1, bd=4)

        self.button_ext = Button(self.frame1, text="Valider", width=10,
                                 command=self.VerifTypeFichier, background='lemon chiffon')

        self.label_ext.grid(row=3, column=0, columnspan=2)
        self.list_ext_txt.grid(row=4, column=0, padx=10, pady=5)
        self.list_ext_html.grid(row=5, column=0, padx=10, pady=5)
        self.list_ext_all.grid(row=6, column=0, padx=10, pady=5)
        self.label_ext_input.grid(row=4, column=1, padx=10, pady=5)
        self.ext_input.grid(row=5, column=1, padx=10, pady=5)
        self.button_ext.grid(row=6, column=1, padx=10, pady=5)

# -------------------- Sélection des répertoires  -------------------- #

        self.frame2 = Frame(master=self, background='DarkSeaGreen1')
        self.frame2.grid_columnconfigure(0, weight=1)
        self.frame2.grid_rowconfigure(0, weight=1)

        self.label_origin = Label(
            self.frame2, text="Sélectionner le dossier source: ", background='DarkSeaGreen1')
        self.button_origin = Button(self.frame2, text="Ouvrir", width=10,
                                    command=self.OpenDirectoryOrigin, background='DarkSeaGreen1')
        self.label_to_synchro = Label(
            self.frame2, text="Sélectionner le dossier cible: ", background='DarkSeaGreen1')
        self.button_to_synchro = Button(
            self.frame2, text="Ouvrir", width=10, command=self.OpenDirectoryToSynchro, background='DarkSeaGreen1')

        self.frame2.grid(row=7, column=0, ipadx=10, ipady=10, sticky='new')
        self.label_origin.grid(row=8, column=0, columnspan=2, padx=10, pady=5)
        self.button_origin.grid(row=9, column=0, columnspan=2, padx=10, pady=5)
        self.label_to_synchro.grid(
            row=10, column=0, columnspan=2, padx=10, pady=5)
        self.button_to_synchro.grid(
            row=11, column=0, columnspan=2, padx=10, pady=5)

# -------------------- Sélection des paramètres ftp  -------------------- #

        self.frame3 = Frame(master=self, background='Pink')
        self.frame3.grid_columnconfigure(0, weight=1)
        self.frame3.grid_rowconfigure(0, weight=1)

        self.label_ip = Label(
            self.frame3, text="Adresse du serveur ftp: ", background='Pink')
        self.ip_input = Entry(self.frame3, bd=4)

        self.label_login = Label(
            self.frame3, text="Login: ", background='Pink')
        self.login_input = Entry(self.frame3, bd=4)

        self.label_pass = Label(
            self.frame3, text="Mot de passe: ", background='Pink')
        self.pass_input = Entry(self.frame3, bd=4)

        self.frame3.grid(row=12, column=0, ipadx=10, ipady=10, sticky='new')
        self.label_ip.grid(row=13, column=0, columnspan=2, padx=5, pady=5)
        self.ip_input.grid(row=13, column=1, columnspan=2, padx=100, pady=5)
        self.label_login.grid(row=14, column=0, columnspan=2, padx=5, pady=5)
        self.login_input.grid(row=14, column=1, columnspan=2, padx=100, pady=5)
        self.label_pass.grid(row=15, column=0, columnspan=2, padx=5, pady=5)
        self.pass_input.grid(row=15, column=1, columnspan=2, padx=100, pady=5)

# -------------------- Bouton d'actions START-STOP-REPRISE -------------------- #

        self.frame4 = Frame(master=self, background='pale turquoise')
        self.frame4.grid_columnconfigure(0, weight=1)
        self.frame4.grid_rowconfigure(0, weight=1)

        self.button_start = Button(
            self.frame4, text="START", command=self.ConfirmStartSynchro, background='pale turquoise')
        self.button_reprise = Button(self.frame4, text="REPRISE",
                                  command=self.RepriseSynchro, background='pale turquoise')
        self.button_stop = Button(self.frame4, text="STOP",
                                   command=self.StopSynchro, background='pale turquoise')

        self.frame4.grid(row=17, column=0, sticky='new')
        self.button_start.grid(row=18, sticky='ew')
        self.button_stop.grid(row=19, sticky='ew')
        self.button_reprise.grid(row=20, sticky='ew')

        self.label_end = Label(self, text="", background='pale turquoise')
        self.label_end.grid(row=21, column=0)

# -------------------- FONCTIONS --------------------------------------------- #

# fonction récupérant le chemin du dossier source
    def OpenDirectoryOrigin(self, event=None):
        self.directory = filedialog.askdirectory()
        self.dossier_origin = self.directory

# fonction récupérant le chemin du dossier cible
    def OpenDirectoryToSynchro(self, event=None):
        if self.type_synchro == "local" :
            self.directory = filedialog.askdirectory()
            self.dossier_cible = self.directory
        else :
            self.dossier_cible = "/"

# fonction affichant les données saisies par l'utilisateur dans un pop up
    def ConfirmStartSynchro(self, event=None):
        msg1 = "Type de synchronisation: %s" % (self.type_synchro)
        msg2 = "\nFichiers spécifiés: %s" % (self.extensions)
        msg3 = "\nDossier source: %s" % (self.dossier_origin)
        msg4 = "\nDossier à synchroniser: %s" % (self.dossier_cible)
        confirm = messagebox.askyesno(
            title="Les données sont-elles correctes ?", message=msg1+msg2+msg3+msg4)
        if confirm:
            # on démarre la synchro
            self.StartSynchro()
        else:
            self

# fonction récupérant le type de synchro (local, ftp)
    def VerifTypeSynchro(self, event=None):
        self.type = self.list_methods.curselection()[0]
        if self.type == 1:
            self.type_synchro = "ftp"
            self.dossier_cible = "/"
            self.ip_input['state'] = 'normal'
            self.login_input['state'] = 'normal'
            self.pass_input['state'] = 'normal'
            self.button_to_synchro['state'] = 'disabled'

        else:
            self.type_synchro = "local"
            self.ip_input['state'] = 'disabled'
            self.login_input['state'] = 'disabled'
            self.pass_input['state'] = 'disabled'
            self.button_to_synchro['state'] = 'normal'

# fonction récupérant les extensions demandées par l'utilisateur
    def VerifTypeFichier(self, event=None):
        self.extensions = []
        if self.ext_input.get() != "":
            self.extensions.append(self.ext_input.get())
        if self.ext_txt.get() == 1:
            self.extensions.append("txt")
        if self.ext_html.get() == 1:
            self.extensions.append("html")
        if self.ext_all.get() == 1:
            self.extensions.append("all")

# fonction démarrant la synchro
    def StartSynchro(self, event=None):
        try:
            # init d'une instance de synchronisation (=> état "non synchro")
            fsql.Init_request_Synchro(self.info_bdd, self.dossier_origin, self.dossier_cible)
            self.info_bdd["curseur"].execute("SELECT last_insert_rowid()")
            self.info_bdd["id_synchro_en_cours"] = self.info_bdd["curseur"].fetchone()[0]

            #########################################
            # exécution de la synchro en mode local #
            #########################################
            if self.type_synchro == "local":

                # lancement du listing
                print("==> listing des dossiers <==")
                dossiers = fl.compare_directories_changes(self.info_bdd, self.dossier_origin, self.dossier_cible)
                time.sleep(2)
                
                print("==> listing des fichiers <==")
                fichiers = fl.compare_files_changes(self.info_bdd, self.dossier_origin, self.dossier_cible, self.extensions)
                time.sleep(2)

                print("==> opérations de synchro <==")
                # Suppression des fichiers
                for fich in fichiers[0]:
                    fs.suppression_fichier(self.info_bdd, fich, self.dossier_cible)
                # Suppression des dossiers
                for doss in dossiers[0]:
                    fs.suppression_dossier(self.info_bdd, doss, self.dossier_cible)
                # Création des dossiers
                for doss in dossiers[1]:
                    fs.ajout_dossier(self.info_bdd, doss, self.dossier_cible)             
                # Création des fichiers
                for fich in fichiers[1]:
                    fs.ajout_fichier(self.info_bdd, fich, self.dossier_origin, self.dossier_cible)
                # Update des fichiers
                for fich in fichiers[2]:
                    fs.modification_fichier(self.info_bdd, fich, self.dossier_origin, self.dossier_cible)

            ###############
            # Synchro ftp #
            ###############
            else :
                # Paramètres de connexion
                host = self.ip_input.get()
                user = self.login_input.get()
                password = self.pass_input.get()

			    # Connexion
                ftp = FTP(host)
                ftp.login(user=user, passwd=password)
                ftp.encoding = "utf-8"

                # lancement du listing
                print("==> listing des dossiers <==")
                dossiers = fl_ftp.compare_directories_changes(self.info_bdd,ftp,self.dossier_origin,self.dossier_cible)
                time.sleep(2)

                print("==> listing des fichiers <==")
                fichiers = fl_ftp.compare_files_changes(self.info_bdd,ftp,self.dossier_origin,self.dossier_cible,self.extensions)
                time.sleep(2)

                print("==> opérations de synchro <==")
                # Suppression des fichiers
                for fich in fichiers[0]:
                    fs_ftp.suppr_fichier_ftp(self.info_bdd,ftp,fich)

                # Suppression des dossiers
                for doss in dossiers[0]:
                    fs_ftp.suppr_dossier_ftp(self.info_bdd,ftp,doss)

                # Création des dossiers
                for doss in dossiers[1]:
                    fs_ftp.ajout_dossier_ftp(self.info_bdd,ftp,doss)
                    
                # Création des fichiers
                for fich in fichiers[1]:
                    fs_ftp.ajout_fichier_ftp(self.info_bdd,ftp,fich, self.dossier_origin)

                # Update des fichiers
                for fich in fichiers[2]:
                    fs_ftp.modification_ftp(self.info_bdd,ftp,fich, self.dossier_origin)

            # on clear la base
            self.label_end['text'] = "Fin de synchro"
            print("==> nettoyage de la bdd <==")
            time.sleep(2)
            fsql.clean_tables(self.info_bdd)
            self.ip_input['state'] = 'normal'
            self.login_input['state'] = 'normal'
            self.pass_input['state'] = 'normal'
            self.button_to_synchro['state'] = 'normal'

        except AttributeError:
            messagebox.showerror(title="Attention !",
                                 message="Veuillez vérifier vos informations")

# fonction qui vérifie qu'il n'existe pas de synchro en cours
    def RepriseSynchro(self, event=None):
        self.info_bdd["curseur"].execute("SELECT num_synchro FROM Synchro WHERE etat_synchro='non synchro'")
        res = self.info_bdd["curseur"].fetchall()
        print("==> listing des fichiers non synchronisés en base <==")
        time.sleep(2)

        # s'il n'y a pas de synchro, on quitte l'appli
        if not(res):
            print("==> il n'y a pas de synchro en cours <==")
            self.label_end['text'] = "Il n'existe pas de synchro en cours"

        # s'il existe des synchros non terminés on les reprends
        else:
            for id in res:
                self.info_bdd["id_synchro_en_cours"] = id[0]
                #on recupere les chemins
                self.info_bdd["curseur"].execute("""
                    SELECT chemin_source 
                    FROM Synchro 
                    WHERE num_synchro=?""",id)
                path_dossier_source = self.info_bdd["curseur"].fetchone()[0]
                self.info_bdd["curseur"].execute("""
                    SELECT chemin_cible 
                    FROM Synchro 
                    WHERE num_synchro=?""",id)
                path_dossier_cible = self.info_bdd["curseur"].fetchone()[0]

                print("==> synchro en cours <==")
                
                # on parcourt les différentes tables pour trouver les fichiers 'non synchro'
                try:
                    # suppression des fichiers
                    self.info_bdd["curseur"].execute("""SELECT nom 
                        FROM Fichier 
                        WHERE etat_fichier='non synchro' 
                            AND operation_fichier='supprimer'
                            AND fk_synchro=?""", id)
                    res = self.info_bdd["curseur"].fetchall()
                    if (res):
                        for fich in res:
                            fs.suppression_fichier(self.info_bdd, fich[0], path_dossier_cible)
                    
                    # suppression des dossiers
                    self.info_bdd["curseur"].execute("""SELECT nom 
                        FROM Dossier 
                        WHERE etat_dossier='non synchro' 
                            AND operation_dossier='supprimer' 
                            AND fk_synchro=?""",id)
                    res = self.info_bdd["curseur"].fetchall()
                    if (res):
                        for doss in res:
                            fs.suppression_dossier(self.info_bdd, doss[0], path_dossier_cible)
                    
                    # création des dossiers
                    self.info_bdd["curseur"].execute("""SELECT nom 
                        FROM Dossier 
                        WHERE etat_dossier='non synchro' 
                            AND operation_dossier='ajouter'
                            AND fk_synchro=?""",id)
                    res = self.info_bdd["curseur"].fetchall()
                    if (res):
                        for doss in res:
                            fs.ajout_dossier(self.info_bdd, doss[0], path_dossier_cible)
             
                    # création des fichiers
                    self.info_bdd["curseur"].execute("""SELECT nom 
                        FROM Fichier 
                        WHERE etat_fichier='non synchro' 
                            AND operation_fichier='ajouter'
                            AND fk_synchro=?""",id)
                    res = self.info_bdd["curseur"].fetchall()
                    if (res):
                        for fich in res:
                            fs.ajout_fichier(self.info_bdd, fich[0], path_dossier_source,path_dossier_cible)

                    # mise à jour des fichiers
                    self.info_bdd["curseur"].execute("""SELECT nom 
                        FROM Fichier 
                        WHERE etat_fichier='non synchro' 
                            AND operation_fichier='modifier'
                            AND fk_synchro=?""",id)
                    res = self.info_bdd["curseur"].fetchall()
                    if (res):
                        for fich in res:
                            fs.modification_fichier(self.info_bdd, fich[0], path_dossier_source, path_dossier_cible)
                except:
                    print("synchro interrompue")
                
                print("==> nettoyage de la bdd <==")
                time.sleep(2)
                fsql.clean_tables(self.info_bdd)
                self.label_end['text'] = "Synchro terminée"

# fonction qui stoppe le programme et le ferme
    def StopSynchro(self, event=None):
        self.info_bdd["curseur"].close()
        self.info_bdd["connect"].close()
        print("Connexion BDD fermée")
        self.destroy()
        exit()
