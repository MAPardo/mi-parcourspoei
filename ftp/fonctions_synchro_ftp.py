#!/usr/bin/env python

from ftplib import *
import bdd.fonctions_sql as fsql

###############################################################################
# On parcourt les dossiers -> On supprime au COMPLET                          #
#                          -> On créé l'arborescence de dossiers              #
# On parcourt les fichiers -> On update                                       #
#                          -> On créé tous les fichiers dans leur destination #
#                          -> On supprime si il reste des récalcitrants       #
###############################################################################

# Update fichier
def modification_ftp(info_bdd,ftp,fichier,path):
    suppr_fichier_ftp(info_bdd, ftp,fichier)
    ajout_fichier_ftp(info_bdd, ftp, fichier, path) 
    fsql.Update_Fichier(info_bdd, fichier)
    print('le fichier ' + fichier + ' est modifié') 

# Ajout dossier vide
def ajout_dossier_ftp(info_bdd,ftp,dossier):
    coupes = dossier.count("/") - 1
    sous_dossiers= dossier.rsplit("/",coupes) # /d1/d2/d3 -> /d1, d2, d3
    inter = sous_dossiers[0][1:]
    sous_dossiers[0] = inter
    for elem in sous_dossiers :
        try :
            ftp.cwd(elem)
        except :
            nom = elem + "/"
            ftp.mkd(nom)
            ftp.cwd(elem)
    ftp.cwd("/")
    fsql.Update_Dossier(info_bdd, dossier)
    print('le dossier ' + dossier + ' est ajouté.')


# Ajout des fichiers
def ajout_fichier_ftp(info_bdd,ftp,fichier,path):
    #découpe du path /d1/d2/d3/f1 -> /d1 d2 d3 f1
    coupes = fichier.count("/") - 1
    sous_dossiers = fichier.rsplit("/",coupes)
    inter = sous_dossiers[0][1:]
    sous_dossiers[0] = inter
    for i in range(len(sous_dossiers)-1):
    #on va dans d1 puis dans d2 puis dans d3
        ftp.cwd(sous_dossiers[i])
    #on copie f1
    nom = path + fichier
    with open(nom, 'rb') as f :
            ftp.storbinary('STOR ' + fichier, f)
    ftp.cwd("/")
    fsql.Update_Fichier(info_bdd, fichier)
    print('le fichier ' + fichier + ' est copié')

# Suppression des fichiers
def suppr_fichier_ftp(info_bdd,ftp,fichier):
    #découpe du path /d1/d2/d3/f1 -> /d1 d2 d3 f1
    coupes = fichier.count("/") - 1
    sous_dossiers = fichier.rsplit("/",coupes)
    inter = sous_dossiers[0][1:]
    sous_dossiers[0] = inter
    for i in range(len(sous_dossiers)-1):
    #on va dans d1 puis dans d2 puis dans d3
        ftp.cwd(sous_dossiers[i])
    #on delete f1
    ftp.delete(fichier)
    fsql.Update_Fichier(info_bdd, fichier)
    print('le fichier ' + fichier + ' est supprimé')
    ftp.cwd("/")    


# Suppression dossier 
def suppr_dossier_ftp(info_bdd,ftp,dossier): # /d1/d2/d3  
    liste = []
    #découpe du path /d1/d2/d3/f1 -> /d1 d2 d3 
    coupes = dossier.count("/") - 1
    sous_dossiers = dossier.rsplit("/",coupes)
    inter = sous_dossiers[0][1:]
    sous_dossiers[0] = inter
    for i in range(len(sous_dossiers)):
    #on va dans d1 puis dans d2 puis dans d3
        ftp.cwd(sous_dossiers[i])
    # On liste d3
    liste = ftp.mlsd()
    # Pour les elem de d3
    for elt in liste :
        # Si c'est un fichier
        if elt[1].get("type")=="file":
            ftp.delete(elt[1].get("nom"))
            fsql.Update_Fichier(info_bdd, elt[1].get("nom"))
            print('le fichier ' + elt[1].get("nom") + ' est supprimé')
        # Si c'est un dossier
        elif elt[1].get("type")=="dir":
            # Je vais dedans et j'appelle
            suppr_dossier_ftp(info_bdd,ftp,elt[1].get("nom"))
    ftp.rmd(dossier)
    fsql.Update_Dossier(info_bdd, dossier)
    print('Le dossier ' + dossier + ' est supprimé.')
    ftp.cwd("/")