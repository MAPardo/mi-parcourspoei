#!/usr/bin/env python

##--------------Importation des Modules --------------##

import os
import shutil
from bdd import fonctions_sql as fsql

##-------------- Définition des fonctions ------------##

#Fonctions de copie
def ajout_fichier(info_bdd, fichier, path_origine, path_cible):
    path_initial = os.path.join(path_origine,fichier[1:])
    path_destination = os.path.join(path_cible,fichier[1:])

    shutil.copy(path_initial,path_destination)
    fsql.Update_Fichier(info_bdd, fichier)
    print('le fichier ' + fichier + ' est copié')

def ajout_dossier(info_bdd, dossier, path_cible):
    path_destination = os.path.join(path_cible,dossier[1:])

    try:
        os.makedirs(path_destination)
        fsql.Update_Dossier(info_bdd, dossier)
    except OSError:
        print ("La création du dossier %s a échoué." % dossier)
    else:
        print('le dossier ' + dossier + ' est ajouté.')


#Fonction écrase

def modification_fichier(info_bdd, fichier, path_origine, path_cible):
    path_initial = os.path.join(path_origine,fichier[1:])
    path_destination = os.path.join(path_cible,fichier[1:])

    shutil.copy(path_initial,path_destination)
    fsql.Update_Fichier(info_bdd, fichier)
    print('le fichier ' + fichier + ' est modifié')   

        
        
#Fonctions de suppression
def suppression_dossier(info_bdd, dossier, path_cible):
    path_destination = os.path.join(path_cible,dossier[1:])

    try:
        shutil.rmtree(path_destination)
        fsql.Update_Dossier(info_bdd, dossier)

    except OSError:
        print ("La suppression du dossier %s a échouée." % dossier)
    else:
        print('Le dossier ' + dossier + ' est supprimé.')



def suppression_fichier(info_bdd, fichier, path_cible):
    path_destination = os.path.join(path_cible,fichier[1:])

    os.remove(path_destination)
    fsql.Update_Fichier(info_bdd, fichier)
    print('le fichier ' + fichier + ' est supprimé')
