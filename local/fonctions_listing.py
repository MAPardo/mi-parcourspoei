#!/usr/bin/env python

##--------------Importation des Modules --------------##

import os
import datetime
import pathlib
from bdd import fonctions_sql as fsql


##-------------- Définition des fonctions ------------##

def Dossier_Existe(dossier):
	return True if os.path.isdir(dossier) and os.path.exists(dossier) else False

def Dossier_Vide(dossier):
	return False if (len(os.listdir(dossier)) != 0) else True

# Prends une liste d'extensions de fichiers au format '<name>.ext' et renvoi 
# une liste contenant uniquement le nom de ces extensions en minuscule 
# ou l'argument complet si le format n'est pas reconnu 
# ou 'True' si un argument est 'all'. 
def Verif_Extensions(extensions):
	ext = []
	for extension in extensions:
		extension.lower()
		if extension == "all":
			return True
		if "." in extension:
			exten = extension.split('.')
			ext.append(exten[1])
		else:
			ext.append(extension)
	return ext


# Prend en entrée le chemin absolu d'un dossier et renvoie une liste
#  de chemin relatif à partir du dossier d'entrée pour les dossiers qui y sont contenus.
def list_directories_content(path):
	dossiers = []
	for root, dirs, files in os.walk(path):
		# Listing des dossiers.
		for d in dirs:
			dir_path = os.path.join(root, d)
			cuted_path = dir_path.split(path)[1]
			clean_path = cuted_path.replace("\\","/")
			dossiers.append(clean_path)
	
	return dossiers


# Prend en entrée le chemin absolu d'un dossier et une liste d'extension de fichiers
# et renvoie une liste de chemin relatif à partir du dossier d'entrée pour les fichiers 
# qui y sont contenus.
def list_files_content(path, extensions):
	fichiers = []
	for root, dirs, files in os.walk(path):
		# Listing des fichiers.
		for f in files:
			file_path = os.path.join(root, f) # chemin absolu
			cuted_path = file_path.split(path)[1] # chemin relatif au dossier d'entrée
			clean_path = cuted_path.replace("\\","/") # chemin corrigé avec uniquement de '/'
			
			# Listing du fichier seulement s'il fait partie des extensions demandées.
			if extensions == True:
				fichiers.append(clean_path)
			else:
				cuted_ext = clean_path.split('.')
				if cuted_ext[1] in extensions:
					fichiers.append(clean_path)
	
	
	return fichiers


# Reçoit en entrée les chemins absolus de deux dossiers (source et cible)
# et renvoie les dossiers à supprimer et à créer dans la cible.
def compare_directories_changes(info_bdd, source, target):
	source_directory_content = list_directories_content(source)
	target_directory_content = list_directories_content(target)

	
## Comparaison des contenus en dossiers.
	# dossiers présents dans la cible mais pas dans la source.
	directory_to_delete = list(set(target_directory_content).difference(source_directory_content))
	# dossiers présents dans la source mais pas dans la cible.
	directory_to_create = list(set(source_directory_content).difference(target_directory_content))

	fsql.Insert_Dossier_SUP(info_bdd, directory_to_delete)
	fsql.Insert_Dossier_ADD(info_bdd, directory_to_create)

	return [directory_to_delete, directory_to_create]


# Reçoit en entrée les chemins absolus de deux dossiers (source et cible) ainsi qu'une liste d'extensions de fichiers 
# et renvoie les fichiers à supprimer, à créer et à modifier dans la cible en fonction des extensions de fichiers données.
def compare_files_changes(info_bdd, source, target, extensions):

	ext = Verif_Extensions(extensions)
	source_file_content = list_files_content(source, ext)
	target_file_content = list_files_content(target, ext)

## Comparaison des contenus en fichiers.
	# fichiers présents dans la cible mais pas dans la source.
	files_to_delete = list(set(target_file_content).difference(source_file_content))
	# fichiers présents dans la source mais pas dans la cible.
	files_to_create = list(set(source_file_content).difference(target_file_content))
	# fichiers présents à la fois dans la source et dans la cible.
	matching_files = list(set(source_file_content).intersection(target_file_content))
	# fichiers plus récents dans la source que dans la cible.
	files_to_update = []  

	for elt in matching_files:

		date_in_source = datetime.datetime.fromtimestamp(
			pathlib.Path(source + elt).stat().st_mtime
		)
		date_in_target = datetime.datetime.fromtimestamp(
			pathlib.Path(target + elt).stat().st_mtime
		)

		if date_in_source > date_in_target:
			files_to_update.append(elt)

	fsql.Insert_Fichier_SUP(info_bdd, files_to_delete)
	fsql.Insert_Fichier_ADD(info_bdd, files_to_create)
	fsql.Insert_Fichier_MODIF(info_bdd, files_to_update)

	return [files_to_delete, files_to_create, files_to_update]