#!/usr/bin/env python
# LANCER SERVEUR LINUX : /etc/init.d/mysql stop

##--------------Importation des Modules --------------##

import sqlite3


##-------------- Variables Globales ------------------##


##-------------- Définition des fonctions ------------##

# -- Effacer contenu des tables -- #
def clean_tables(info_bdd):
    info_bdd["curseur"].execute("""
        DELETE FROM Synchro 
        WHERE num_synchro=?
        """, (info_bdd["id_synchro_en_cours"],))
    info_bdd["connect"].commit()

    info_bdd["curseur"].execute("""
        DELETE FROM Dossier 
        WHERE fk_synchro=?
        """, (info_bdd["id_synchro_en_cours"],))
    info_bdd["connect"].commit()

    info_bdd["curseur"].execute("""
        DELETE FROM Fichier 
        WHERE fk_synchro=?
        """,(info_bdd["id_synchro_en_cours"],))
    info_bdd["connect"].commit()

##-----------------------------------------------------##

# -- Création d'une instance de synchro -- #
def Init_request_Synchro(info_bdd, dossier_source, dossier_cible):
    data = []
    data.append(dossier_source)
    data.append(dossier_cible)
    info_bdd["curseur"].execute("""
        INSERT INTO Synchro (chemin_source, chemin_cible, type_synchro, etat_synchro)
        VALUES (?, ?, "local", "non synchro")
    """, data)
    info_bdd["connect"].commit()

##-----------------------------------------------------##

# -- Inertions des données en base -- #
def Insert_Dossier_SUP(info_bdd, data_dossier):
    data =[]
    for i in data_dossier:
        data.append((i,'supprimer', 'non synchro', info_bdd["id_synchro_en_cours"])) 
    info_bdd["curseur"].executemany("""
        INSERT INTO Dossier(nom, operation_dossier, etat_dossier, fk_synchro)
        VALUES (?, ?, ?, ?)
    """, data)
    info_bdd["connect"].commit()

def Insert_Dossier_ADD(info_bdd, data_dossier):
    data =[]
    for i in data_dossier:
        data.append((i,'ajouter', 'non synchro', info_bdd["id_synchro_en_cours"])) 
    info_bdd["curseur"].executemany("""
        INSERT INTO Dossier(nom, operation_dossier, etat_dossier, fk_synchro)
        VALUES (?, ?, ?, ?)
    """, data)
    info_bdd["connect"].commit()

def Insert_Fichier_SUP(info_bdd,data_fichier):
    data =[]
    for i in data_fichier:
        data.append((i, 'supprimer','non synchro', info_bdd["id_synchro_en_cours"])) 
    info_bdd["curseur"].executemany("""
        INSERT INTO Fichier(nom, operation_fichier, etat_fichier, fk_synchro)
        VALUES (?, ?, ?, ?)
    """, data)
    info_bdd["connect"].commit()

def Insert_Fichier_ADD(info_bdd,data_fichier):
    data =[]
    for i in data_fichier:
        data.append((i, 'ajouter','non synchro', info_bdd["id_synchro_en_cours"])) 
    info_bdd["curseur"].executemany("""
        INSERT INTO Fichier(nom, operation_fichier, etat_fichier, fk_synchro)
        VALUES (?, ?, ?, ?)
    """, data)
    info_bdd["connect"].commit()

def Insert_Fichier_MODIF(info_bdd,data_fichier):
    data =[]
    for i in data_fichier:
        data.append((i, 'modifier','non synchro', info_bdd["id_synchro_en_cours"])) 
    info_bdd["curseur"].executemany("""
        INSERT INTO Fichier(nom, operation_fichier, etat_fichier, fk_synchro)
        VALUES (?, ?, ?, ?)
    """, data)
    info_bdd["connect"].commit()

##-----------------------------------------------------##

# -- Mise à jour des états -- #
def Update_Dossier(info_bdd, dossier):
    info_bdd["curseur"].execute("""
        UPDATE Dossier 
        SET etat_dossier='synchro'
        WHERE nom=?
    """, (dossier,))
    info_bdd["connect"].commit()

def Update_Fichier(info_bdd, fichier):
    info_bdd["curseur"].execute("""
        UPDATE Fichier 
        SET etat_fichier='synchro'
        WHERE nom=?
    """, (fichier,))
    info_bdd["connect"].commit()

