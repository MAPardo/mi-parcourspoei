CREATE TABLE IF NOT EXISTS Synchro (
    num_synchro INTEGER PRIMARY KEY,
    chemin_source TEXT NOT NULL,
    chemin_cible TEXT NOT NULL,
    type_synchro TEXT CHECK( type_synchro IN ('local','ftp','sftp') ) NOT NULL DEFAULT '-',
    etat_synchro TEXT CHECK( etat_synchro IN ('non synchro', 'en cours') ) NOT NULL
);

CREATE TABLE IF NOT EXISTS Dossier(
    id_dossier INTEGER PRIMARY KEY,
    nom TEXT NOT NULL,
    operation_dossier TEXT CHECK( operation_dossier IN ('supprimer','ajouter') ) NOT NULL,
    etat_dossier TEXT CHECK( etat_dossier IN ('non synchro','synchro') ) NOT NULL,
    fk_synchro INTEGER NOT NULL,
    FOREIGN KEY(fk_synchro) REFERENCES Synchro(num_synchro)
);

CREATE TABLE IF NOT EXISTS Fichier(
    id_fichier INTEGER PRIMARY KEY,
    nom TEXT NOT NULL,
    operation_fichier TEXT CHECK( operation_fichier IN ('supprimer','ajouter','modifier') ) NOT NULL,
    etat_fichier TEXT CHECK( etat_fichier IN ('non synchro','synchro') ) NOT NULL,
    fk_synchro INTEGER NOT NULL,
    FOREIGN KEY(fk_synchro) REFERENCES Synchro(num_synchro)
);