use LOCATION;

-- Pour chaque habitation, affichez son code, son type, la ville où elle se trouve 
-- les noms deslocataires et leurs professions
create or replace view v_habitation as 
	select C.Nom, L.Codeh, Ville, Typeh, Profession
	from HABITATION H
	inner join LOCATION L on H.Codeh=L.Codeh
	inner join CLIENT C on L.Nom=C.Nom
	order by H.Codeh;

select * from v_habitation;

--  La même que la requête précédente mais affichez aussi les habitations qui n'ont jamais été louées
create or replace view v_habitation_all as 
	select H.Codeh, Typeh, Ville, C.Nom, Profession
	from HABITATION H
	left join LOCATION L on H.Codeh=L.Codeh
	left join CLIENT C on L.Nom=C.Nom
	order by H.Codeh;

select * from v_habitation_all;

-- Déterminez les minimum, maximum et moyenne de loyer des habitations pour chaque type et chaque ville.
create or replace view v_loyers_stats as 
	select Ville, Typeh, min(LoyerM), Max(LoyerM), avg(LoyerM)
	from HABITATION
	group by Ville, Typeh
	order by Ville asc;
	
select * from v_loyers_stats;

-- Pour chaque type d'habitation, déterminez le nombre d'habitations de ce type qui ont été prises en location
create or replace view v_nb_par_type as 
	select distinct Typeh, count(*)
	from LOCATION L, HABITATION H
	where L.Codeh=H.CodeH
	group by Typeh
	order by Typeh asc;

select * from v_nb_par_type;

-- Même consigne que la précédente, mais affichez seulement les types dont au moins 4 habitations (pas forcément différentes) ont été louées
create or replace view v_nb_par_type_4 as 
	select distinct Typeh, count(*)
	from LOCATION L, HABITATION H
	where L.Codeh=H.CodeH
	group by Typeh
	having count(*)>=4
	order by Typeh;

select * from v_nb_par_type_4;

-- Déterminez le nombre total des mois de location de chaque habitation. Facultatif : pour celles qui n'ont jamais été louées, affichez la valeur 0 (utilisez la fonction ifnull)
create or replace view v_nb_mois_loc as 
	Select Typeh, H.Codeh, IFNULL(NombMois, 0)
	from LOCATION L 
	right join HABITATION H on L.Codeh=H.CodeH
	order by H.Codeh;

select * from v_nb_mois_loc;

-- Pour chaque client, calculez ses frais totaux de loyer.
create or replace view v_frais_client as 
	select Nom, sum(LoyerM*NombMois)
	from LOCATION L, HABITATION H
	where L.Codeh=H.CodeH
	group by Nom
	order by Nom asc;

select * from v_frais_client;

-- Trouvez les clients qui n'ont jamais loué aucune habitation
create or replace view v_non_locataire as 
	Select C.Nom 
	from CLIENT C
	left join LOCATION L on C.Nom=L.Nom
	where L.Codeh is null;

select * from v_non_locataire;

-- Trouvez les clients qui ont loué à la fois des appartements de type 1 et des appartements de type 3. (Utilisez exists).
create or replace view v_TYPE_1_3 as 
	Select Nom,  
	from LOCATION L
	inner join HABITATION H on L.Codeh=H.Codeh
	where H.Typeh='TYPE1' and exists (
		select Nom
		from LOCATION L
		inner join HABITATION H on L.Codeh=H.Codeh
		where H.Typeh='TYPE3');

select * from v_TYPE_1_3;

