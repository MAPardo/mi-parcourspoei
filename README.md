# Projet POEI mi-parcours

Ce mini projet de mi-parcours de la formation POEI vous permet de synchroniser vos fichiers entre deux dossiers de votre choix.

## Features

Mettez à jour un dossier cible depuis un dossier source:
- Entre deux dossiers en local sur votre machine.
- Depuis un dossier local vers un dossier distant grâce au protocole ftp.
- Arrêtez votre synchronisation et reprenez là quand vous voulez.
- Le tout directement en ligne de commande ou via une interface graphique.

## Technologies

Ce projet est écrit avec le langage [Python3].
Il contient une base de données [SQLite] stockée en local sur votre poste sous la forme du fichier _synchro.db_. Ce fichier se génère automatiquement lors de votre 1ère utilisation.
Pour une synchronisation avec un répertoire distant, le projet utilise le protocole [FTP].

## Installation

Ce projet a été développé uniquement sous Linux (Ubuntu 20.04) et Windows (10). Il est recommandé d'utiliser un de ces systèmes pour un fonctionnement optimal.

Ce projet nécessite d'avoir Python 3 pour fonctionner. 
Ensuite vous devez installer [Tkinter] (pour l'interface graphique) et SQLite3 (pour la base de données).

Sur Linux:
```sh
sudo apt-get install python3-tk
sudo apt-get install sqlite3
```

Sur Windows
https://tkdocs.com/tutorial/install.html#install-win-python
https://www.sqlite.org/download.html

Puis les modules Python suivant:
```sh
pip install python-dateutil
```

Récupérer ensuite le répertoire git sur votre machine:
```sh
mkdir mon_nouveau_dossier
cd mon_nouveau_dossier
git clone https://gitlab.com/MAPardo/mi-parcourspoei.git
```

## Utilisation
#### Rapide description du programme
Lors d'une nouvelle synchronisation, le programme commence par créer une instance de synchronisation dans la base de données.
Ensuite il liste les dossiers/fichiers à synchroniser et sauvegarde leur nom en base avec un statut 'non synchro' sous l'instance de synchronisation précédement créée.
Puis il s'occupe des traitements à faire sur ces dossiers et fichiers. A chaque fin de traitement, il passe le statut du dossier/fichier à 'synchro'.
Si tous les dossiers/fichiers ont été traités, le programme nettoie la base de données en supprimant toutes les entrées dossiers/fichiers liées à l'instance.

Si le programme se coupe lors du traitement des dossiers/fichiers, il est possible de reprendre la synchronisation.
Le programme va alors parcourir la base de données pour chercher s'il existe une instance de synchronisation. Si oui, il va effectuer les traitements sur tous les dossiers/fichiers liés à cette instance et dont le statut est 'non synchro'. Une fois tous dossiers/fichiers traités, le programme nettoie la base de données en supprimant toutes les entrées dossiers/fichiers liées à l'instance.

Cette reprise n'est correcte que si le programme a eu le temps de lister les dossiers/fichiers en base lors d'une première tentative de synchronisation.

#### En ligne de commandes
- Syntaxe:
```sh
python3 main_inline.py [arguments]
```
- Terminer les synchronisations en cours:
```sh
python3 main_inline.py
```
- Synchroniser en local:
```sh
python3 main_inline.py [chemin_dossier_source] [chemin_dossier_cible] [extension] 'local'
``` 
- Synchroniser à distance:
```sh
python3 main_inline.py [chemin_dossier_source] [extension] 'ftp'
```
=> Un menu va ensuite vous guider dans le paramètrage de la synchronisation.

**Exemple de synchronisation:**
1. On veut mettre à jour le dossier "cible" contenu dans le dossier "jeu_de_test" par rapport au dossier "source" contenu lui aussi dans "jeu_de_test". C'est donc une synchronisation en local.
De plus on ne veut synchroniser que les fichiers portant l'extension ".html".
   ```sh
   python3 main_inline.py 'jeu_de_test/source' 'jeu_de_test/cible' 'html' 'local'
   ```
2. Dans le cas de la synchronisation à distance on va mettre à jour le dossier racine du serveur ftp. Il n'y a donc pas d'argument pour le dossier cible. Les paramètres de connexion au serveur ftp (adresse, login, password) sont demandés via des input lors de l'exécution du programme.
Ici la racine du serveur est déjà initialisée avec le dossier jeu_de_test/cible et tous les fichiers sont synchronisés.
   ```sh
   python3 main_inline.py 'jeu_de_test/source' 'all' 'ftp'
   ```

#### Avec l'interface graphique
- Syntaxe:
```sh
python3 main_tk.py
```
1. L'interface graphique se lance.
2. On choisi le type de synchronisation et on valide.
3. On choisi les extensions des fichiers à prendre en compte et on valide.
**Si on a choisi une synchronisation locale:**
-On renseigne le chemin du dossier source.
-On renseigne le chemin du dossier à synchroniser par rapport au dossier source.
**Si on a choisi une synchronisation à distance:**
-On renseigne le chemin du dossier source.
-On renseigne les informations nécessaires à la connexion FTP.
5. On démarre la synchronisation en appuyant sur START.
6. On vérifie les informations du pop-up et on valide.
7. Si l'on souhaite arrêter la synchronisation, on appuie sur le bouton STOP.

*Pour reprendre une synchronisation non terminée, on appuie sur le bouton REPRISE.*

## Notes
- Pour synchroniser tout les fichiers, mettre 'all' en tant qu'extension.
- Pour arrêter une synchronisation en ligne de commande, tuez le processus ou CTRL-C.

## Architecture

```
mi-parcoursPOEI
│   README.md
│   main_inline.py
│   main_tk.py
│
└───local
│    │   __init__.py
│    │   fonctions_listing.py
│    │   fonctions_synchro.py
│
└───graphic
│    │   __init__.py
│    │   fenetre.py
│
└───ftp
│    │   __init__.py
│    │   fonctions_listing_ftp.py
│    │   fonctions_synchro_ftp.py
│
└───bdd
│    │   __init__.py
│    │   fonctions_sql.py
│    │   script.sql
│
└───jeu_de_test
      └───source
      |      | [...]
      |       
      └───cible
             | [...]
```

[SQLite]: <https://www.sqlite.org/index.html>
[Python3]: <https://www.python.org/download/releases/3.0/>
[FTP]: <https://fr.wikipedia.org/wiki/File_Transfer_Protocol>
[Tkinter]: <https://coderslegacy.com/python/python-gui/>

## Auteurs
@Geoffrey-Dintilhac
@Victor-Mackowski
@Emma-Pardo

mars - avril 2021
