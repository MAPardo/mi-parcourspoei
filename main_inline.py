#!/usr/bin/env python

##--------------Importation des Modules --------------##
import local.fonctions_listing as fl
import local.fonctions_synchro as fs
import bdd.fonctions_sql as fsql
from ftplib import FTP
import ftp.fonctions_synchro_ftp as fs_ftp
import ftp.fonctions_listing_ftp as fl_ftp
import sys
import sqlite3
import time

##-------------- Variables Globales ------------------##

path_dossier_source=""
path_dossier_cible=""
fichiers = []
info_bdd = {}
ext = []

##-------------- Programme ---------------------------##
# démarrer le programme: python3 main_inline.py 'chemin dossier source' 'chemin dossier cible' ['local', 'ftp']
##----------------------------------------------------##
# 1. ouverture de la bdd
# 2. affichage du menu
# 3. choix de l'utilisateur
# 3.1 l'utilisateur lance une nouvelle synchro
# 3.2 l'utilisateur termine les synchro en cours
# 3.3 l'utilisateur quitte le programme
##----------------------------------------------------##

# connexion à la BDD
try:
    con = sqlite3.connect('synchro.db')
    cursor = con.cursor()
    print("Successfully Connected to SQLite")
    with open("bdd/script.sql") as queryfile:
        cursor.executescript(queryfile.read())
    info_bdd["connect"] = con
    info_bdd["curseur"] = cursor
    print("Script de création des tables exécuté")
except (FileNotFoundError, sqlite3.OperationalError) as e:
	print(e)

# menu
choix=True
while choix:
    print("""
    1.Démarrer une nouvelle synchronisation
    2.Terminer les synchronisations en cours
    3.Quitter
    """)
    choix=input("Que souhaitez-vous faire ? ")

# CHOIX 1 : déroulement classique du programme    
    if choix=="1":
        ##################
        # Synchro locale #
        ##################
        if len(sys.argv) == 5 and sys.argv[4]=='local': 
            path_dossier_source = sys.argv[1].replace("\\","/")
            path_dossier_cible = sys.argv[2].replace("\\","/")
            ext.append(sys.argv[3])

            if fl.Dossier_Existe(path_dossier_source) and fl.Dossier_Existe(path_dossier_cible):
                if fl.Dossier_Vide(path_dossier_source) == False:

                    # init d'une instance de synchronisation (=> état "non synchro")
                    fsql.Init_request_Synchro(info_bdd, path_dossier_source, path_dossier_cible)
                    cursor.execute("SELECT last_insert_rowid()")
                    info_bdd["id_synchro_en_cours"] = cursor.fetchone()[0]

                    # lancement du listing
                    print("==> listing des dossiers <==")
                    dossiers = fl.compare_directories_changes(info_bdd, path_dossier_source, path_dossier_cible)
                    time.sleep(2)
                    print("==> listing des fichiers <==")
                    fichiers = fl.compare_files_changes(info_bdd, path_dossier_source, path_dossier_cible, ext) 
                    time.sleep(2)

                    print("==> opérations de synchro <==")
                    # Suppression des fichiers
                    for fich in fichiers[0]:
                        fs.suppression_fichier(info_bdd, fich, path_dossier_cible)

                    # Suppression des dossiers
                    for doss in dossiers[0]:
                        fs.suppression_dossier(info_bdd, doss, path_dossier_cible)

                    # Création des dossiers
                    for doss in dossiers[1]:
                        fs.ajout_dossier(info_bdd, doss, path_dossier_cible)
                    
                    # Création des fichiers
                    for fich in fichiers[1]:
                        fs.ajout_fichier(info_bdd, fich, path_dossier_source,path_dossier_cible)

                    # Update des fichiers
                    for fich in fichiers[2]:
                        fs.modification_fichier(info_bdd, fich, path_dossier_source, path_dossier_cible)

                    #-------------------------------------------------------------------------------------------------------
                    
                    print("==> nettoyage de la bdd <==")
                    time.sleep(2)
                    fsql.clean_tables(info_bdd)

                    cursor.close()
                    con.close()
                    print("Connexion BDD fermée")
                    exit()
                else:
                    print("Votre dossier source est vide")
                    cursor.close()
                    con.close()
                    print("Connexion BDD fermée")
                    exit()
            else:
                print("Veuillez donner un chemin valide")
                cursor.close()
                con.close()
                print("Connexion BDD fermée")
                exit()
        ###############        
        # Synchro ftp #
        ###############
        elif len(sys.argv) == 4 and sys.argv[3]=='ftp': # Pas de target à donner

            # Paramètres de connexion
            host = input("Quelle est l'adresse du serveur ? ")
            user = input("Quel est votre login ? ")
            password = input("Quel est votre password ? ")

			# Connexion
            ftp = FTP(host)
            ftp.login(user=user, passwd=password)
            ftp.encoding = "utf-8"

            path_dossier_source = sys.argv[1].replace("\\","/")
            ext = sys.argv[2]
            path_dossier_cible = ftp.pwd() # "/" du serveur

            if fl_ftp.Dossier_Existe(path_dossier_source) :
                if fl_ftp.Dossier_Vide(path_dossier_source) == False:

                    # init d'une instance de synchronisation (=> état "non synchro")
                    fsql.Init_request_Synchro(info_bdd, path_dossier_source, path_dossier_cible)
                    cursor.execute("SELECT last_insert_rowid()")
                    info_bdd["id_synchro_en_cours"] = cursor.fetchone()[0]

                    # lancement du listing
                    print("==> listing des dossiers <==")
                    dossiers = fl_ftp.compare_directories_changes(info_bdd,ftp,path_dossier_source, path_dossier_cible)
                    time.sleep(2)
                    print("==> listing des fichiers <==")
                    fichiers = fl_ftp.compare_files_changes(info_bdd,ftp,path_dossier_source, path_dossier_cible,ext)
                    time.sleep(2)

                    print("==> opérations de synchro <==")
                    # Suppression des fichiers
                    for fich in fichiers[0]:
                        fs_ftp.suppr_fichier_ftp(info_bdd,ftp,fich)

                    # Suppression des dossiers
                    for doss in dossiers[0]:
                        fs_ftp.suppr_dossier_ftp(info_bdd,ftp,doss)

                    # Création des dossiers
                    for doss in dossiers[1]:
                        fs_ftp.ajout_dossier_ftp(info_bdd,ftp,doss)
                    
                    # Création des fichiers
                    for fich in fichiers[1]:
                        fs_ftp.ajout_fichier_ftp(info_bdd,ftp,fich, path_dossier_source)

                    # Update des fichiers
                    for fich in fichiers[2]:
                        fs_ftp.modification_ftp(info_bdd,ftp,fich, path_dossier_source)

                    #-------------------------------------------------------------------------------------------------------

                    print("==> nettoyage de la bdd <==")
                    time.sleep(2)
                    fsql.clean_tables(info_bdd)

                    cursor.close()
                    con.close()
                    print("Connexion BDD fermée")
                    exit()

                else:
                    print("Votre dossier source est vide")
                    cursor.close()
                    con.close()
                    print("Connexion BDD fermée")
                    exit()
            else:
                print("Veuillez donner un chemin valide")
                cursor.close()
                con.close()
                print("Connexion BDD fermée")
                exit()
			# Fermeture de la connexion
            ftp.quit()
        else:
            print("Veuillez donner des paramètres valides")
            print("Local : [path_source] [path_cible] [ext_fichier] local")
            print("FTP : [path_source] [ext_fichier] ftp")
            cursor.close()
            con.close()
            print("Connexion BDD fermée")
            exit()

# CHOIX 2 : vérification qu'il n'existe pas de synchro en cours
    elif choix=="2":
        cursor.execute("SELECT num_synchro FROM Synchro WHERE etat_synchro='non synchro'")
        res = cursor.fetchall()
        print("==> listing des fichiers non synchronisés en base <==")
        time.sleep(2)
        if not(res):
            print("==> il n'y a pas de synchro en cours <==")
            cursor.close()
            con.close()
            print("Connexion BDD fermée")
            exit()
        else:
            # s'il existe des synchros non terminés on les reprends
            for id in res:
                info_bdd["id_synchro_en_cours"] = id[0]
                #on recupere les chemins
                cursor.execute("""
                    SELECT chemin_source 
                    FROM Synchro 
                    WHERE num_synchro=?""",id)
                path_dossier_source = cursor.fetchone()[0]
                cursor.execute("""
                    SELECT chemin_cible 
                    FROM Synchro 
                    WHERE num_synchro=?""",id)
                path_dossier_cible = cursor.fetchone()[0]

                print("==> synchro en cours <==")
                
                
                try:
                    # suppression des fichiers
                    cursor.execute("""SELECT nom 
                        FROM Fichier 
                        WHERE etat_fichier='non synchro' 
                            AND operation_fichier='supprimer'
                            AND fk_synchro=?""", id)
                    res = cursor.fetchall()
                    if (res):
                        for fich in res:
                            fs.suppression_fichier(info_bdd, fich[0], path_dossier_cible)
                    
                    # suppression des dossiers
                    cursor.execute("""SELECT nom 
                        FROM Dossier 
                        WHERE etat_dossier='non synchro' 
                            AND operation_dossier='supprimer' 
                            AND fk_synchro=?""",id)
                    res = cursor.fetchall()
                    if (res):
                        for doss in res:
                            fs.suppression_dossier(info_bdd, doss[0], path_dossier_cible)
                    
                    # création des dossiers
                    cursor.execute("""SELECT nom 
                        FROM Dossier 
                        WHERE etat_dossier='non synchro' 
                            AND operation_dossier='ajouter'
                            AND fk_synchro=?""",id)
                    res = cursor.fetchall()
                    if (res):
                        for doss in res:
                            fs.ajout_dossier(info_bdd, doss[0], path_dossier_cible)
                                        
                    # création des fichiers
                    cursor.execute("""SELECT nom 
                        FROM Fichier 
                        WHERE etat_fichier='non synchro' 
                            AND operation_fichier='ajouter'
                            AND fk_synchro=?""",id)
                    res = cursor.fetchall()
                    if (res):
                        for fich in res:
                            fs.ajout_fichier(info_bdd, fich[0], path_dossier_source,path_dossier_cible)

                    # mise à jour des fichiers
                    cursor.execute("""SELECT nom 
                        FROM Fichier 
                        WHERE etat_fichier='non synchro' 
                            AND operation_fichier='modifier'
                            AND fk_synchro=?""",id)
                    res = cursor.fetchall()
                    if (res):
                        for fich in res:
                            fs.modification_fichier(info_bdd, fich[0], path_dossier_source, path_dossier_cible)
                except:
                    print("synchro interrompue")
                
                print("==> nettoyage de la bdd <==")
                time.sleep(2)
                fsql.clean_tables(info_bdd)

                cursor.close()
                con.close()
                print("Connexion BDD fermée")
                exit()

# CHOIX 3 : on quitte le programme            
    elif choix=="3":
        cursor.close()
        con.close()
        print("Connexion BDD fermée")
        exit()
    else:
       print("\n Choix incorrect")
