#!/usr/bin/env python

##--------------Importation des Modules --------------##

import graphic.fenetre as fe
import platform
import sqlite3

##-------------- Variables Globales ------------------##

path_dossier_source=""
path_dossier_cible=""
fichiers = []
info_bdd = {}

##-------------- Programme ---------------------------##

# Fermeture de la BDD à la fermeture de la fenêtre
def on_closing():
    cursor.close()
    con.close()
    print("Connexion BDD fermée")
    app.destroy()
    exit()
    
# connexion à la BDD
try:
    con = sqlite3.connect('synchro.db')
    cursor = con.cursor()
    print("Successfully Connected to SQLite")
    with open("bdd/script.sql") as queryfile:
        cursor.executescript(queryfile.read())
    info_bdd["connect"] = con
    info_bdd["curseur"] = cursor
    print("Script de création des tables exécuté")
except (FileNotFoundError, sqlite3.OperationalError) as e:
	print(e)

# Gestion de l'affichage:
if platform.system() == "Linux":
    taille_fenetre = '1000x1200'
elif platform.system() == "Windows":
    from ctypes import windll
    # Set DPI Awareness (Windows 10 and 8) => corrige le flou du à la résolution de l'écran de Windows supérieur
    windll.shcore.SetProcessDpiAwareness(1)
    taille_fenetre = '500x800'
else:
    taille_fenetre = '1000x1200'

# Création de la fenêtre
app = fe.Application(info_bdd)
app.geometry(taille_fenetre)
app.title("Module de Synchro")
app.configure(background='pale turquoise')
app.columnconfigure(0, weight=1)
app.grid_columnconfigure(0, weight=1)
app.protocol("WM_DELETE_WINDOW", on_closing)
app.mainloop()
